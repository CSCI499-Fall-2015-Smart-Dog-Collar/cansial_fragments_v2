package com.example.smna.cansialapp2;


import android.app.Activity;
import android.os.Bundle;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

public class Graph extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.graph);

        GraphView graph = (GraphView) findViewById(R.id.graph);
        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(new DataPoint[] {
                new DataPoint(0, 20),
                new DataPoint(1, 24),
                new DataPoint(2, 30),
                new DataPoint(3, 27),
                new DataPoint(4, 26),
                new DataPoint(5, 28),
                new DataPoint(6, 26),
                new DataPoint(7, 26),
                new DataPoint(8, 26),
        });

        graph.addSeries(series);
    }
}

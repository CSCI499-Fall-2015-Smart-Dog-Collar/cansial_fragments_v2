package com.example.smna.cansialapp2;

//import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class LoginFragment extends Fragment {

    private EditText username;
    private EditText password;

    private Button btn_signin;
    private Button btn_register;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.login_frag, container, false);

        // set username & password
        username = (EditText)v.findViewById(R.id.ask_username);
        password = (EditText)v.findViewById(R.id.ask_password);
        // set buttons
        btn_signin = (Button)v.findViewById(R.id.btn_sign_in);
        btn_register = (Button)v.findViewById(R.id.btn_register);

        // button listeners
        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // do sign in method
                // if successful, then go to profile fragment
                Toast.makeText(getActivity(), "Sign in success", Toast.LENGTH_SHORT).show();
            }
        });

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // do register method
                Toast.makeText(getActivity(), "Register success", Toast.LENGTH_SHORT).show();
            }
        });

        return v;
    }

}
package com.example.smna.cansialapp2;


//import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;


public class ProfileFragment extends Fragment {

    // are used to show the profile information
    TextView get_users_name;
    TextView get_dogs_name;
    TextView get_dogs_age;
    TextView get_dogs_breed;
    TextView get_dogs_weight;

    Button btn_graph;

    ImageView imageView;
    String dogPicUrl = "http://static.ddmcdn.com/gif/earliest-dogs-660x433-130306-akita-660x433.jpg";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.profile_frag, container, false);
        btn_graph = (Button) v.findViewById(R.id.btn_graph);
        btn_graph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context c = getContext();
                Intent intent = new Intent(c, Graph.class);
                c.startActivity(intent);
            }
        });
        // place to set dog picture
        imageView = (ImageView) v.findViewById(R.id.dog_pic);

        // start async tasks
        // GetProfileInfo().execute(url1, url2, url3, url4, url5);
        new GetDogPic().execute(dogPicUrl);

        return v;
    } // onCreateView


    // asynctask to get picture under the header "dog profile", there is imageview for picture of dog
    private class GetDogPic extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected void onPreExecute () {
            Log.i("AsyncTask-dogPic", "onPreExecute called");
        }

        // update the view the profile
        @Override
        protected void onPostExecute (Bitmap result) {
            Log.i("AsyncTask-dogPic", "onPostExecute called");
            imageView.setImageBitmap(result);
        }

        // get dog picture
        @Override
        protected Bitmap doInBackground (String... param) {
            return downloadBitmap (param[0]);
        }

        // do http client here
        private Bitmap downloadBitmap (String url) {
        // initialize http client object
        final DefaultHttpClient client = new DefaultHttpClient();
        final HttpGet getRequest = new HttpGet(url);

        try {
            HttpResponse response = client.execute(getRequest);
            final int statusCode = response.getStatusLine().getStatusCode();

            if (statusCode != HttpStatus.SC_OK) {
                Log.e("ImageDownloader", "Error " + statusCode + " while retrieving bitmap from " + url);
				return null;
            }

            final HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream inputStream = null;
                try {
                    // get contents from stream
                    inputStream = entity.getContent();
                    final Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                    return bitmap;
                } finally {
                    // cleanup
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    // close HttpEntity
                    entity.consumeContent();
                }
            }
        } catch (Exception e) {
            getRequest.abort();
            Log.e("ImageDownloader", "Something went wrong while retrieving bitmap from " + url + e.toString());
            final String errorMsg = e.getMessage();
            Toast.makeText(getActivity(), "Error Downloading", Toast.LENGTH_SHORT).show();
        }

        return null;
        } // downloadBitmap
    } // async

/*
    // async task to get user & dog information different from task from getting dog picture
    private class GetProfileInfo extends AsyncTask <String, String, String[]> {
        // 5 profile info field to get
        String[] urlArray = new String[5];

        @Override
        protected void onPreExecute () {
            Log.i("AsyncTask-dogPic", "onPreExecute called");
        }

        @Override
        protected String[] doInBackground (String... param) {

            for (int i=0; i<params.length; i++) {
                // http client
                DefaultHttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
                HttpGet httpGet = new HttpGet(params[i]);
                HttpResponse response;

                try {
                    response = httpClient.execute(httpGet);
                    urlArray[i] = EntityUtils.toString(response.getEntity());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } // for
            return urlArray;
        } // doInBackground

        @Override
        protected void onPostExecute (String[] result) {
            Log.i("AsyncTask-dogPic", "onPostExecute called");

            // update profile info to view
            try {
                // parse JSON
                // result = result.substring(3);
                // Log.d(TAG, "JSON Corrected Data" + result);

                JSONArray jArray = new JSONArray(result);
                JSONObject[] jsonArr = new JSONObject[5];
                for (int j=0; j<=urlArray.length; j++) {
                    jsonArr[j] = jArray.getJSONObject(j);
                }

                // find textviews and set profile info
                TextView tv1 = (TextView) getView().findViewById(R.id.get_users_name);
                TextView tv2 = (TextView) getView().findViewById(R.id.pets_name);
                TextView tv3 = (TextView) getView().findViewById(R.id.age_of_pet);
                TextView tv4 = (TextView) getView().findViewById(R.id.breed_of_pet);
                TextView tv5 = (TextView) getView().findViewById(R.id.weight_of_pet);

                tv1.setText(jsonArr[0].getString());
                tv2.setText(jsonArr[1].getString());
                tv3.setText(jsonArr[2].getString());
                tv4.setText(jsonArr[3].getString());
                tv5.setText(jsonArr[4].getString());


            } catch (Exception e) {
                e.printStackTrace();
            }
        } // onPost
    } // finish asyncTask

     */
} // UserProfileFragment
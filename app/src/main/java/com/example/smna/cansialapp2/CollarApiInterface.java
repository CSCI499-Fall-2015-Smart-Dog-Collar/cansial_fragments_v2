/*
 * Copyright (C) 2015 Kevin Guan, Daniel Hrabovcak
 *
 * This file is for private use only. If you did not receive this file from the original author
 * listed above, you must delete it immediately.
 */


package com.example.smna.cansialapp2;
import android.os.AsyncTask;
import android.util.Log;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Takes in a parameter that consist of the users' data. The data gets concatenated with our server
 * parameters. The parameters are encoded and then the server response gets parsed. When server
 * responds with 200, a GET, POST or UPDATE response was valid. Otherwise, it will throw an
 * exception.
 *
 * @author Kevin Guan
 * @version 1.0
 * @since 2015-11-15
 *
 */
abstract public class CollarApiInterface
{
    static final protected String DEBUG_TAG = "CollarApiInterface-Debug";

    /**
     * The website's API URL.
     */
    static final private String WEBSITE_URL = "https://csci499-hunter-2015-collar.herokuapp.com/api";

    /**
     * Used inside AsyncTask to determine type of errors.
     */
    public enum Error
    {
        /**
         * Indicates that there was no error.
         */
        NoError,

        /**
         * Indicates that there was an error creating the parameter string.
         */
        ParameterStringError,

        /**
         * Indicates that there was an error parsing server response into an object.
         */
        ObjectParseError

    }

    /**
     * Returns the parameter for GET requests.
     */
    abstract String parameterGet();

    /**
     * Converts the given response string to a specified parsed object.
     */
    abstract Object objectFromJsonGet(String json);

    /**
     * It first takes a parameter, a string, that is concatenated a specific way, otherwise, it will
     * result in a HttpURLConnection error. It is then concatenated to the back of WEBSITE_URL and
     * will be used to send a GET response from the server. A response to the server is established,
     * and the response returns as a JSON Object and returned.
     * @return              The Async task which connects to the server and returns the parse result.
     * @throws Exception    MalformedUrlException, which is thrown to indicate that a
     *                      malformed URL has occurred. Either no legal protocol could be found in
     *                      a specification string or the string could not be parsed. IOException,
     *                      Signals that an I/O exception of some sort has occurred. This class is
     *                      the general class of exceptions produced by failed or interrupted I/O
     *                      operations.
     */
    ParseServerObjectTask responseGet() throws Exception
    {
        String query = parameterGet();
        HttpURLConnection conn = (HttpURLConnection) new URL(WEBSITE_URL + "?" + query).openConnection();

        conn.setRequestMethod("GET");

        ParseServerObjectTask task = new ParseServerObjectTask();
        task.execute(conn);

        Log.d(DEBUG_TAG, "" + task.getStatus());
        return task;
    }

    /**
     * Establish connection to server. If the connection is a success, it will respond with code
     * 200, otherwise, it will give another numeric response. Then it will request a response from
     * the server and return it back as a string.
     * @param conn      A URL that will be used to check if a connection can be made to it.
     * @return          The response from the server.
     */
    private static String sendRequest(HttpURLConnection conn)
    {
        InputStream is = null;

        try
        {
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.connect();     // Starts the query

            if (conn.getResponseCode() != 200)
            {
                Log.d(DEBUG_TAG, "Connection response: failure");
                return "";
            }

            is = conn.getInputStream();
            String contentAsString = convertStreamToString(is);

            return contentAsString;
        }

        catch(Throwable t)
        {
            Log.d(DEBUG_TAG, "Exception: " + t.toString() + t.getMessage());
        }
        finally
        {
            if (is != null)
            {
                try
                {
                    is.close();
                }
                catch (IOException ie)
                {
                    Log.e(DEBUG_TAG, "IOException " + ie);
                }
            }
        }

        return "Connection is not working!";
    }

    /**
     * Given a URL, establishes an HttpUrlConnection and retrieve the web page content as a
     * InputStream, which it returns as a string.
     * @param is        webpage downloaded as inputstream
     * @return          the length of the inputstream
     */
    private static String convertStreamToString(InputStream is)
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
        finally
        {
            try
            {
                is.close();
            }
            catch (IOException ioe)
            {
                ioe.printStackTrace();
            }
        }
        return sb.toString();
    }

    /**
     * Uses AsyncTask to create a task away from the main UI thread. This task takes a URL string
     * and uses it to create an HttpUrlConnection. Once the connection has been established,
     * the AsyncTask downloads the contents of the webpage as an InputStream. Finally, the
     * InputStream is converted into a string, which is displayed in the UI by the AsyncTask's
     * onPostExecute method.
     */
    public class ParseServerObjectTask extends AsyncTask<HttpURLConnection, Void, Object> implements com.example.smna.cansialapp2.DownloadWebpageTask {
        private Error err;

        @Override
        protected Object doInBackground(HttpURLConnection... urls)
        {
            err = Error.NoError;

            String parameters = parameterGet();
            if (parameters.isEmpty())       // Make this return empty if error.
            {
                err = Error.ParameterStringError;
                return null;
            }

            JSONObject jsonParam;

            String response = sendRequest(urls[0]);

            Object obj =  objectFromJsonGet(response);

            if (obj == null)
            {
                err = Error.ObjectParseError;
                return null;
            }

            return obj;
        }

        public Error getError()
        {
            return err;
        }
    }

}

package com.example.smna.cansialapp2;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;


public class TrackFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    Button btnSOS, btnRoute;
    SupportMapFragment mapFragment;
    private View v;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // to prevent duplicating fragment views conflicts which forces closes
        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null) {
                parent.removeView(v);
            }
        }

        try {
            v = inflater.inflate(R.layout.track_frag, container, false);
        } catch (InflateException e) { } // map is already initialized, return as it is



        btnSOS = (Button) v.findViewById(R.id.btn_locate);
        btnRoute = (Button) v.findViewById(R.id.btn_show_route);

        // set listeners for buttons
        btnSOS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Getting Last Known Location", Toast.LENGTH_SHORT).show();

                // onMapReady(mMap);
            }
        });
        btnRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Showing Route History", Toast.LENGTH_SHORT).show();

                // get last 4 gps coordinates

                // LatLng loc1 = new LatLng();
                // LatLng loc2 = new LatLng();
                // LatLng loc3 = new LatLng();
                // LatLng loc4 = new LatLng();
                // onMapReady(mMap, loc1, loc2, loc3, loc4);
            }
        });

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.gmap);
        mapFragment.getMapAsync(this);

        return v;
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        GoogleMapOptions options = new GoogleMapOptions();


 /*
        // check if mMap has location set. If not then it's from track button and
        // get the latest gps coordinate from server and parse the JSON object.
        double lat = ;
        double lng = ;
        LatLng currentLoc = new LatLng(lat, lng);
        mMap.addMarker(new MarkerOptions().position(currentLoc).title("Current Location"));
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.setMyLocationEnabled(true);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLoc, 15));
*/


        // Add a marker in hunter college
        // the gps coordinate is hardcoded
        LatLng hc = new LatLng(40.768872, -73.964656);
        mMap.addMarker(new MarkerOptions().position(hc).title("Hunter College"));
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.setMyLocationEnabled(true);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(hc, 15));


        /*
        // this adds a straight line without caring for streets
        PolylineOptions polylineOptions = new PolylineOptions()
                .add(new LatLng(40.768872, -73.964656))
                .add(new LatLng(40.788872, -73.974656))
                .add(new LatLng(40.768872, -73.984656))
                .color(Color.BLUE)
                .width(7)
                .geodesic(true);

        Polyline polyline = mMap.addPolyline(polylineOptions);
        */

    }


    public void onMapReadyRoute (GoogleMap googleMap, LatLng loc1, LatLng loc2, LatLng loc3, LatLng loc4) {
        mMap = googleMap;
        GoogleMapOptions options = new GoogleMapOptions();

    }

}

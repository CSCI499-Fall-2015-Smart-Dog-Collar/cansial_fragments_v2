package com.example.smna.cansialapp2;


//import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class UserProfileFragment extends Fragment {

    // are used to show the profile information
    TextView get_users_name;
    TextView get_dogs_name;
    TextView get_dogs_age;
    TextView get_dogs_breed;
    TextView get_dogs_weight;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.profile_frag, container, false);

        // start async task
        //

        return v;
    } // onCreateView


    /*
    // asynctask to get picture under the header "dog profile", there is imageview for picture of dog
    private class GetDogPic extends AsyncTask <String, Void, Bitmap> {
        @Override
        protected void onPreExecute () {}

        @Override
        protected void onPostExecute (Bitmap result) {}

        // get dog picture
        @Override
        protected Bitmap doInBackground (String... param) {
            return Bitmap downloadBitmap (param[0]);
        }

        // do http client here
        private Bitmap downloadBitmap (String url) {}
    }


    // async task to get user & dog information different from task from getting dog picture
    private class GetProfileInfo extends AsyncTask <String, String, String> {
        @Override
        protected void onPreExecute () {}

        @Override
        protected void onPostExecute (String[] result) {}

        @Override
        protected String doInBackground (String... param) {}

    }

     */
} // UserProfileFragment
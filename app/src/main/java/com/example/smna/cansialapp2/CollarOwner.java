package com.example.smna.cansialapp2;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import java.net.URLEncoder;

/**
 * The CollarOwner class accesses the users login information.
 * This allow them to login and view their personal information.
 *
 * @author Kevin Guan
 * @version 1.0
 * @since 2015-11-03
 */

public class CollarOwner extends CollarApiInterface
{
    private String email;
    private String password;
    private String gender = null;
    private String name_first = null;
    private String name_middle = null;
    private String name_last = null;
    private String address = null;
    private String phone_work = null;
    private String phone_cellular = null;
    private String phone_home = null;
    private String parameter_update = null;

    CollarOwner(String email, String password)
    {
        // Constructor that takes in email and password during application login.
        this.email = email;
        this.password = password;
    }

    /**
     * Asks the server for updated owner information.
     * @return A parameter including email and password.
     */
    @Override
    String parameterGet()
    {
        try
        {
            String charset = "UTF-8";
            String query = String.format("owner?email=%s&password=%s",
                    URLEncoder.encode(email, charset),
                    URLEncoder.encode(password, charset));
            return query;
        }
        catch(Exception e)
        {
            Log.e(DEBUG_TAG, "" + e);
            return "owner?email=" + email + "&" + "password=" + password;
        }
    }

    /**
     *
     * @param json Parses an owner JSON string.
     * @return A new CollarOwner.
     */
    @Override
    Object objectFromJsonGet(String json)
    {
        JSONObject jsonParam;
        try
        {
            jsonParam = new JSONObject(json);
        }
        catch (JSONException je)
        {
            return null;
        }

        CollarOwner owner = new CollarOwner(email, password);
        try
        {
            owner.gender = jsonParam.getString("gender");
        }
        catch(Exception e)
        {
            return null;
        }
        return owner;
    }

    // TODO
    //Java Doc for proper Java documentation FORMAT IT THIS WAY

    public void email(String email)
    {
        this.email = email;
    }

    public String getEmail()
    {
        return email;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPassword()
    {
        return password;
    }

    public void setGender(String gender)
    {
        this.gender = gender;
    }

    public String getGender()
    {
        return gender;
    }

    public void setNameFirst(String name_first)
    {
        this.name_first = name_first;
    }

    public String getNameFirst() {return name_first;}

    public void setNameMiddle(String name_middle)
    {
        this.name_middle = name_middle;
    }

    public String getNameMiddle()
    {
        return name_middle;
    }

    public void setNameLast(String name_last)
    {
        this.name_last = name_last;
    }

    public String getNameLast() {return name_last;}

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getAddress()
    {
        return address;
    }

    public void setPhoneWork(String phone_work)
    {
        this.phone_work = phone_work;
    }

    public String getPhoneWork()
    {
        return phone_work;
    }

    public void setPhoneCellular(String phone_cellular)
    {
        this.phone_cellular = phone_cellular;
    }

    public String getPhoneCellular()
    {
        return phone_cellular;
    }

    public void setPhoneHome(String phone_home)
    {
        this.phone_home = phone_home;
    }

    public String getPhoneHome()
    {
        return phone_home;
    }

    public void setParameterUpdate(String parameter_update)
    {
        this.parameter_update = parameter_update;
    }

    public String getParameterUpdate()
    {
        return parameter_update;
    }


}
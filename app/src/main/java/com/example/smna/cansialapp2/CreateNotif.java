package com.example.smna.cansialapp2;


import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class CreateNotif extends Activity {

    private EditText create_notif_title;
    private EditText create_notif_msg;
    private Button btn_create_notif;

    private Notification.Builder mBuilder;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notif);
        create_notif_title = (EditText) findViewById(R.id.title);
        create_notif_msg = (EditText) findViewById(R.id.msg);
        btn_create_notif = (Button) findViewById(R.id.btn_create_notif);

        // cancel all notification when you click it
        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).cancelAll();

        // create notification
        mBuilder = new Notification.Builder(this);
        mBuilder.setSmallIcon(R.mipmap.dog_icon);

        // open the app when notificaiton is clicked
        Intent resultIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        mBuilder.setContentIntent(pendingIntent);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(alarmSound);
    }

    public void create(View view) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mBuilder.setContentTitle(create_notif_title.getText());
        mBuilder.setContentText(create_notif_msg.getText());

        // trigger the notification & clean the text fields
        notificationManager.notify(1, mBuilder.build());
        create_notif_title.getText().clear();
        create_notif_msg.getText().clear();
    }
}

package com.example.smna.cansialapp2;

// import android.app.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class SettingsFragment extends Fragment {

    private ImageButton btn_logout;
    private Button btn_notif;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.settings_frag, container, false);

        btn_logout = (ImageButton) v.findViewById(R.id.pref_logout);
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // write code to log out

                Toast.makeText(getActivity(), "You're logged out", Toast.LENGTH_SHORT).show();
            }
        });


        btn_notif = (Button) v.findViewById(R.id.pref_notification);
        btn_notif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context c = getContext();
                Intent intent = new Intent(c, CreateNotif.class);

                c.startActivity(intent);
            }
        });

        return v;
    }
}


package com.example.smna.cansialapp2;

//import android.app.FragmentManager;
import android.content.Context;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
//import android.app.FragmentTransaction;
import android.support.v4.app.FragmentTransaction;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.IOException;


public class MainActivity extends AppCompatActivity {

    // variables for navigation drawer
    private String[] options_for_drawer;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;


    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // commenting this section of code allows the navigation drawer to
        // open in landscape mode since changing the phone's orientation,
        // onCreate is called again to redraw the view.
        /*
        // check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.fragment_container) != null) {
            // however, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments
            if (savedInstanceState != null) {
                return;
            }
        }
        */

        // initializing navigation drawer
        options_for_drawer = getResources().getStringArray(R.array.navigation_options);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mDrawerList = (ListView)findViewById(R.id.left_drawer);
        //set the adapter for the list view
        mDrawerList.setAdapter(new ArrayAdapter<String>(
                this, R.layout.drawer_list_item, options_for_drawer));
        // set the adapter for the list view
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());


        // listen for open and close events
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            // called when a drawer has settled in a completely closed state
            public void onDrawerClosed (View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            // called when a drawer has settled in a completely open state
            public void onDrawerOpened (View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };


        // set the drawer toggle as the DrawerListener
        //mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        // for the hamburger icon instead of arrow icon for nav drawer
        getSupportActionBar().setLogo(R.mipmap.ic_drawer);

        // for handling fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        // first screen will be login fragment
        LoginFragment loginFragment = new LoginFragment();
        fragmentTransaction.add(R.id.fragment_container,loginFragment);
        fragmentTransaction.commit();

    } // onCreate

    // called whenever we call invalidateOptionsMenu()
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if the navigation drawer is open, hide action item related to the content view
        return super.onPrepareOptionsMenu(menu);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    // swaps fragments in the main content view
    private void selectItem (int position) {
        /*
            switch to different options in the navigation drawer positions should
            be strictly correlated manually.
            e.g., if the first item is social, then case 0 should have the code to
            open the social fragment class. If we want to add more features to nav drawer,
            then we need to edit/add string array in the strings.xml file.
        */
        switch (position) {
            case 0: // profile
                ProfileFragment profileFragment = new ProfileFragment();
                // insert new fragment by replacing any existing fragment
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentTransaction.replace(R.id.fragment_container, profileFragment, "profile")
                        .addToBackStack("back").commit();

                // highlight the selected item, close the drawer
                // after it replaces the fragment
                mDrawerList.setItemChecked(position, true);
                mDrawerLayout.closeDrawer(mDrawerList);
                break;
            case 1: // social network
                SocialFragment socialFragment = new SocialFragment();
                // insert new fragment by replacing any existing fragment
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentTransaction.replace(R.id.fragment_container, socialFragment, "social")
                        .addToBackStack("back").commit();

                // highlight the selected item, close the drawer
                // after it replaces the fragment
                mDrawerList.setItemChecked(position, true);
                mDrawerLayout.closeDrawer(mDrawerList);
                break;
            case 2: // track dog

                TrackFragment trackFragment = new TrackFragment ();
                // insert new fragment by replacing any existing fragment

                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentTransaction.replace(R.id.fragment_container, trackFragment, "track")
                        .addToBackStack("back").commit();

                // highlight the selected item, close the drawer
                // after it replaces the fragment
                mDrawerList.setItemChecked(position, true);
                mDrawerLayout.closeDrawer(mDrawerList);
                break;
            case 3: // settings
                SettingsFragment settingsFragment = new SettingsFragment();
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentTransaction.replace(R.id.fragment_container, settingsFragment, "setting")
                        .addToBackStack("back").commit();

                // highlight the selected item, close the drawer
                // after it replaces the fragment
                mDrawerList.setItemChecked(position, true);
                mDrawerLayout.closeDrawer(mDrawerList);
                break;
        }

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        // handle your other action bar items
        return super.onOptionsItemSelected(item);
    }

    // addToBackStack is need with this method so that fragment manager can know the
    // previous fragment and rightly go back to the previous screen
    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();


        // if the nav drawer is open, then this closes the nav drawer
        if (this.mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.mDrawerLayout.closeDrawer(GravityCompat.START);
        }

        // if the nav drawer is closed, then check if there are prev fragments,
        // which is greater than 0, goes back to previous fragment
        else if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        }

        // else use regular function of back button
        else super.onBackPressed();
    }

}
